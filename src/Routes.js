import React from "react";
import { Route, Routes } from "react-router-dom";

import Home from "./pages/Home";
import Header  from "./components/Header";
import Loginpage from "./pages/Loginpage";
import Welcome from "./pages/Welcome";
import ParkingBooking from "./pages/ParkingBooking";
import Footer from "./components/Footer";


function Routing() {
    return (
        <div>
        <Header/>
        <Routes>
        <Route exact path="/" element={<Welcome />} />
        <Route exact path="/parkingbooking" element={<ParkingBooking />} />
      
      </Routes>
      <Footer/>
      </div>
    );
  }



export default Routing;
