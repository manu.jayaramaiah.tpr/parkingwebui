import React from 'react'
import './header.css'
import Grid from '@mui/material/Grid';
import logo from '../assets/Logo.png';


function Footer() {
  return (
    <div className='footer'>
      <Grid container>
        <Grid item xs={12} sm={1} md={1} lg={1}>
          <img src={logo} width='50px' height='50px' style={{margin : '1vh 0 0 2vw'}}></img>
        </Grid>
        <Grid item xs={12} sm={1} md={1} lg={1}>
          <div className='headerText' style={{margin : '2vh 0 0 0'}}> PARKING SOLUTIONS</div>
        </Grid>
        <Grid item xs={12} sm={9} md={9} lg={9}>
          <span  style={{margin : '0 0 0 8vw',  color : 'white'}} > CONTACT US</span>
          <span  style={{margin : '0 0 0 8vw',  color : 'white'}}> HOW DOES IT WORK</span>
          <span  style={{margin : '0 0 0 8vw',  color : 'white'}}> OUR PRODUCTS</span>
        </Grid>
        <Grid item xs={12} sm={1} md={1} lg={1}>
          <div className='headerText' style={{margin : '4vh 0 0 0'}}>
            FOLLOW US
          </div>
        </Grid>
      </Grid>
    </div>
  )
}

export default Footer