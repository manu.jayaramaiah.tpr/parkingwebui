import React from 'react'
import Grid from '@mui/material/Grid';
import './header.css'
import logo from '../assets/Logo.png';

function Header() {
  return (
    <div className='headerTextHolder'>
      <Grid container>
        <Grid item xs={12} sm={1} md={1} lg={1}>
          <img src={logo} width='50px' height='50px' style={{margin : '1vh 0 0 2vw'}}></img>
        </Grid>
        <Grid item xs={12} sm={10.2} md={10.2} lg={10.2}>
          <div className='headerText' style={{margin : '4vh 0 0 0'}}> PARKING SOLUTIONS</div>
        </Grid>
        <Grid item xs={12} sm={0.8} md={0.8} lg={0.8}>
          <div className='headerText' style={{margin : '4vh 0 0 0'}}>
            contact us
          </div>
        </Grid>
      </Grid>
    </div>

  )
}

export default Header