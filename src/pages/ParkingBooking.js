import React, { useState } from 'react'
import Card from '@mui/material/Card';
import { Button } from "@mui/material";
import Grid from '@mui/material/Grid';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import TwoWheelerIcon from '@mui/icons-material/TwoWheeler';
import Rating from '@mui/material/Rating';
import './css.css'
import background from '../assets/BG.png';
import selectedparkinglot from '../assets/Black Select.png';

import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';


function ParkingBooking(category, location) {

    const [startDate, setStartDate] = useState('')
    const [startTime, setStartTime] = useState('')
    const [lotSelected, setLotSelected] = useState(true)

    return (
        <div style={{ backgroundColor: '#FFB600' }}>
            <Grid container>
                <Grid item xs={12} sm={8} md={8} lg={8} style={{ margin: '10vh 0 0 0' }}>
                    <div style={{ margin: '2vh 0 0 2vw',fontSize: '17.5px' }}> Selected Car Category : car</div>
                    <div  style={{ margin: '2vh 0 0 2vw',fontSize: '17.5px'  }}>Selected Location : </div>
                    <div  style={{ margin: '2vh 0 0 2vw',fontSize: '17.5px'  }}> 
                    <img src = {background} height = '300px' width = '820px' ></img>
                    </div>
                    <div  style={{ margin: '2vh 0 0 2vw',fontSize: '17.5px'  }}>Top Parking Places Near By : location</div>
                    <div  style={{ margin: '2vh 0 0 2vw' }}>
                        <Card >
                            <Grid container>
                                <Grid item xs={12} sm={2} md={2} lg={2} >
                                    <Avatar
                                        style={{ margin: '2vh 0 2vh 2.5vw' }}
                                        sx={{ width: 80, height: 80, bgcolor: '#ffebee' }}
                                    >
                                        R
                                    </Avatar>
                                </Grid>
                                <Grid item xs={12} sm={8} md={8} lg={8} >
                                    <Rating name="read-only" readOnly />
                                    <div style={{ fontSize: '10px' }}>Selected Location :</div>
                                    <div style={{fontSize: '10px' }}>Total avilable spaces :</div>
                                    <div style={{fontSize: '10px' }}>cost :</div>
                                    <div style={{fontSize: '10px' }}> Location :</div>
                                </Grid>
                                <Grid item xs={12} sm={1} md={1} lg={1} >
                                {
                                    lotSelected ?  <img src = {selectedparkinglot}
                                    height = '30px' width = '30px'
                                    style={{ margin: '5vh 0 0 0' }}
                                    /> : '.'
                                }
                               
                                </Grid>
                            </Grid>
                        </Card>
                    </div>

                </Grid>

                <Grid item xs={12} sm={4} md={4} lg={4}>
                    <Card style={{ margin: '12vh 2vw 0 2vw' }}>
                        <CardContent style={{ textAlign: 'center' }}>
                            <div style={{ fontSize: '15px', marginTop: '1vh' }} >
                                Your Parking Details
                            </div>
                            <div style={{ fontSize: '10px', marginTop: '3vh' }} >
                                <span>
                                    <DirectionsCarIcon
                                        style={{ marginRight: '3vh' }}
                                    />
                                </span>
                                <span>
                                    <TwoWheelerIcon />
                                </span>
                            </div>

                            <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                You Have Selected : Car
                            </div>
                            <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                Parking Spot Name : PK parking
                            </div>
                            <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                Cost Per Hour : 100$
                            </div>

                        </CardContent>
                    </Card>

                    <Card style={{ margin: '2vh 2vw 0 2vw' }}>
                        <CardContent style={{ textAlign: 'center' }}>
                            <div style={{ fontSize: '15px' }} >
                                Please Provide Details
                            </div>
                            <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                Vehicle Number
                            </div>
                            <div style={{ marginTop: '1vh' }} >
                                <TextField
                                    size='small'
                                    placeholder='Vehicle Number'
                                    style={{ fontSize: '10px', display: 'inline-block' }}
                                    id="input-with-icon-textfield"
                                    variant="outlined"
                                />
                            </div>
                            <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                Please Enter The Date And Time
                            </div>
                            <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                <LocalizationProvider dateAdapter={AdapterDayjs}>
                                    <Grid container style={{ textAlign: 'center', marginLeft: '4.5vw' }}>
                                        <Grid item xs={10} sm={4} md={4} lg={4}   >
                                            <DesktopDatePicker
                                                // label="Start Date"
                                                inputFormat="MM/DD/YYYY"
                                                value={startDate}
                                                // onChange={(e) => setStartDate(e.target.value)}
                                                onChange={(e) => {
                                                    setStartTime({ didexpirationdate: e })
                                                }}
                                                renderInput={(params) => <TextField size="small" {...params} />}
                                            />
                                        </Grid>

                                        <Grid item xs={10} sm={4} md={4} lg={4}>
                                            <TimePicker
                                                // label="Start Time"
                                                value={startTime}
                                                // onChange={(e) => setStartTime(e.target.value)}
                                                onChange={(e) => {
                                                    setStartTime({ didexpirationdate: e })
                                                }}
                                                renderInput={(params) => <TextField size="small" {...params} />}
                                            />
                                        </Grid>
                                    </Grid>
                                </LocalizationProvider>
                            </div>
                        </CardContent>
                    </Card>

                    <Card style={{ margin: '2vh 2vw 0 2vw' }} >
                        <CardContent style={{ textAlign: 'center' }}>
                            <div style={{ fontSize: '15px', textAlign: 'center' }} >
                                Estimated Cost
                            </div>

                            <Grid container>
                                <Grid item xs={12} sm={6} md={6} lg={6} style={{ textAlign: 'left' }}>

                                    <div style={{ fontSize: '10px', }} >
                                        Parking type :
                                    </div>

                                    <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                        Start Date and Start Time :
                                    </div>
                                    <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                        Cost Per Hour :
                                    </div>
                                </Grid>
                                <Grid item xs={12} sm={6} md={6} lg={6} style={{ textAlign: 'right' }}>
                                    <div style={{ fontSize: '10px', marginTop: '1vh' }}>
                                        Car Parking
                                    </div>
                                    <div style={{ fontSize: '10px', marginTop: '1vh' }} >
                                        23/11/2022 8:30 Am
                                    </div>

                                    <div style={{ fontSize: '10px', marginTop: '1vh' }}>
                                        1$
                                    </div>
                                </Grid>


                            </Grid>
                        </CardContent>
                    </Card>
                    <Button
                        style={{ margin: '2vh 2vw 10vh 2vw', width: '29vw', height: '10vh' }}
                        color="primary"
                        variant="contained"
                    // onClick={() => <ParkingBooking category={category} location={location} />}
                    >Pay Now</Button>
                </Grid>
            </Grid>
        </div >
    )
}

export default ParkingBooking