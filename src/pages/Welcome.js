import React, { useState } from 'react'
import Card from '@mui/material/Card';
import { Button } from "@mui/material";
import Grid from '@mui/material/Grid';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import InputAdornment from '@mui/material/InputAdornment';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import TwoWheelerIcon from '@mui/icons-material/TwoWheeler';
import './css.css'
import ParkingBooking from './ParkingBooking';
import { Outlet, Link } from "react-router-dom";
import background from '../assets/BG.png';
import selectgreen from '../assets/circleselect.png';



function Welcome() {

  const [car, setCar] = useState(false)
  const [bike, setBike] = useState(false)
  const [location, setLocation] = useState('')


  return (
    <div style={{ backgroundImage: `url(${background})` }}>

      <div className="cardHolder">

        <Grid item xs={12} sm={4} md={4} lg={4} >
          <Card style={{ alignItems: 'center', margin: '8vh 0 0 0' }}>
            <CardHeader
              avatar={
                <Avatar
                  sx={{ bgcolor: '#ffebee' }} aria-label="recipe"
                  style={{ fontSize: '20px', margin: '1vh 0 0 15vw' }}
                >
                  R
                </Avatar>
              }
            />
            <CardContent style={{ textAlign: 'center' }}>
              <div style={{ fontSize: '15px' }} >
                Welcome Back !!! name
              </div>
              <div style={{ fontSize: '15px', marginTop: '3vh' }} >
                Please Select The Category
              </div>
              <div style={{ fontSize: '15px', marginTop: '3vh' }} >
                <Grid container>
                  <Grid item xs={12} sm={4} md={4} lg={4}>
                  </Grid>
                  <Grid item xs={12} sm={2} md={2} lg={2}>
                    <DirectionsCarIcon
                     fontSize="large"
                      onClick={() => setCar(!car)}
                    />
                  {/* { car ?? <img src={selectgreen} width='20px' height='20px'></img> } */}
                  <img src={selectgreen} width='20px' height='20px'></img>
                  </Grid>
                  <Grid item xs={12} sm={2} md={2} lg={2}>
                    <TwoWheelerIcon
                     fontSize="large"
                      onClick={() => setBike(!bike)}
                    />
                   {bike ?? <img src={selectgreen} width='20px' height='20px'></img> }
                  </Grid>
                  <Grid item xs={12} sm={4} md={4} lg={4}>
                  </Grid>
                </Grid>
              </div>
              <div style={{ fontSize: '15px', marginTop: '3vh' }} >
                Please Enter your Desired Location
              </div>
              <div style={{ fontSize: '15px', marginTop: '3vh' }} >

                <TextField
                  size='small'
                  style={{ fontSize: '15px' }}
                  id="input-with-icon-textfield"
                  onChange={(e) => setLocation(e.target.value)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <LocationOnIcon />
                      </InputAdornment>
                    ),
                  }}
                  variant="outlined"
                />

              </div>
              <div style={{ fontSize: '15px', marginTop: '3vh' }} >

                <Button color="primary"
                  variant="contained"
                ><Link to="/parkingbooking" style={{ textDecoration: 'none' }}>submit</Link></Button>
              </div>

            </CardContent>
          </Card>
        </Grid>

      </div>


    </div >
  )
}

export default Welcome